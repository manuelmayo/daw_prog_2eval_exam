#1ºDAW MODULAR
#EXAMEN 2º EVAL
#E.1.1 "Hoy comemos mejillones"

def hoy_comemos_mejillones(conchas):
	"""
	Dado un conjunto de números que representan el tamaño de la concha,
	devuelve el número mínimo de montones necesarios para el óptimo uso del
	espacio del plato. Teniendo en cuenta que amontonamos las conchas poniendo
	siempre una más pequeña sobre otra más grande
	
	Args:
		conchas (list,tuple,set...): Conjunto de conchas según el tamaño
	Returns:
		(int): Número mínimo de montones necesarios.
		
	>>> hoy_comemos_mejillones([6,5,4,3])
	1
	>>> hoy_comemos_mejillones([3,4,5,5,5])
	5
	>>> hoy_comemos_mejillones([7,7,7,5,2,5,2,2])
	3
		
	"""
	montones = []
	for c in conchas:
		hai_sitio = False
		for m in montones:
			if c < m[len(m)-1]:
				m.append(c)
				hai_sitio = True
				break
		if not hai_sitio:
			montones.append([c])
	return len(montones)
	
#test
if __name__ == '__main__':
	import doctest
	doctest.testmod(verbose=True)