#1ºDAW MODULAR
#EXAMEN 2º EVAL
#E.1.2

import random
from Mayo_Lado_Manuel_e11 import hoy_comemos_mejillones

def generador_mejillones():
	"""
	A partir de un número introducido por el usuario genera una lista de
	ese número de mejillones de diferentes tamaños. Esta lista la pasará como
	argumento a la función <hoy_comemos_mejillones> y nos mostrará por consola
	el número de montones. Esto se repite indefinidamente en un bucle que se
	podrá finalizar con Ctrl-C.
	"""

	while 1:
		try:
			num = input("\nCuántos mejillones vas a comer [Ctrl-C para salir]? ")
			num = int(num)
		except KeyboardInterrupt:
			print("\nBuen provecho!")
			exit()
		except:
			num = 0
		if num and num > 0:
			mejillones = [random.randint(1,9) for x in range(num)]
			print("Los tamaños son: {}".format(mejillones))
			montones = hoy_comemos_mejillones(mejillones)
			print("Montones resultantes: {}".format(montones))
			
generador_mejillones()