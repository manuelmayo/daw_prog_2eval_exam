#1ºDAW MODULAR
#EXAMEN 2º EVAL
#E.2.2

from Mayo_Lado_Manuel_e21 import *

#1.	Crear una instancia de la clase <Agenda>
agenda = Agenda()

#2.	Añadir a la agenda los siguientes contactos
contacto_p1 = Personal("Jonh Doe",666234545,"05/02/1993")
contacto_p2 = Personal("Tomas Muster",555121212,"tm@gmail.com")
contacto_p3 = Personal("Sarah Chang",444000000,"sarachg@yahoo.com","10/12/1980")

contacto_t1 = Trabajo("Elon Musk",888284848,"boss@spacex.com","SpaceX")
contacto_t2 = Trabajo("Guido van Rossum",999010101,"Dropbox")

agenda.nuevo_contacto(contacto_p1)
agenda.nuevo_contacto(contacto_p2)
agenda.nuevo_contacto(contacto_p3)
agenda.nuevo_contacto(contacto_t1)
agenda.nuevo_contacto(contacto_t2)


#3.	Usar el método <lista_contactos()> de agenda para mostrar los contactos
print("\n3. Todos los contactos:\n")
agenda.lista_contactos()

#4.	Usar el método <busca_contacto()> de agenda para encontrar aquellos 
#	contactos en cuyo nombre se encuentre el texto "mus"
print("\n4. Contactos en cuyo nombre se encuentre 'mus':\n")
agenda.busca_contacto("mus")