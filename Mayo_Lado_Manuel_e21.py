#1ºDAW MODULAR
#EXAMEN 2º EVAL
#E.2.1

#AGENDA
class Agenda:
	
	def __init__(self,contactos=[]):
		if all([isinstance(x,(Personal,Trabajo)) for x in contactos]):
			self.contactos = contactos
		else:
			raise ValueError("El argumento debe ser un conjunto de Contactos")
			
	def nuevo_contacto(self,contacto):
		if isinstance(contacto,(Personal,Trabajo)):
			self.contactos.append(contacto)
		else:
			raise ValueError("El argumento debe ser una instancia de las "
							"clases Personal o Trabajo")
			
	def busca_contacto(self,nombre):
		for c in self.contactos:
			if nombre.upper() in c.nombre.upper():
				print(c)
				
	def lista_contactos(self):
		for c in self.contactos:
			print(c)
	
#CONTACTO
class Contacto:

	def __init__(self,nombre,tlf,email=""):
		self.nombre = nombre
		self.tlf = tlf
		self.email = email
		
	def get_nombre(self):
		return self.nombre
		
	def get_tlf(self):
		return self.tlf
	
	def get_email(self):
		return self.email
		
	def set_nombre(self,nombre):
		self.nombre = nombre
		
	def set_tlf(self,tlf):
		self.tlf = tlf
		
	def set_email(self,email):
		self.email = email
	
#CONTACTO => PERSONAL
class Personal(Contacto):

	def __init__(self,nombre,tlf,opt1="",opt2=""):
		email = ""
		fnac = ""
		for opt in (opt1,opt2):
			if all(x in opt for x in ["@","."]):
				email = opt
			elif all([x.isdigit() for x in opt.split("/")]):
				fnac = opt
		Contacto.__init__(self,nombre,tlf,email)
		self.fnac = fnac
		
	def __str__(self):
		texto = "[{}]\nnombre: {}\ntlf: {}".format("P",self.nombre,self.tlf)
		if self.email:
			texto += "\ne-mail: {}".format(self.email)
		if self.fnac:
			texto += "\ncumpleaños: {}".format(self.fnac)
		return texto
	
#CONTACTO => TRABAJO
class Trabajo(Contacto):

	def __init__(self,nombre,tlf,opt1="",opt2=""):
		email = ""
		empresa = ""
		for opt in (opt1,opt2):
			if all(x in opt for x in ["@","."]):
				email = opt
			elif opt:
				empresa = opt
		if empresa:
			Contacto.__init__(self,nombre,tlf,email)
			self.empresa = empresa
		else:
			raise TypeError("Necesitas añadir una empresa")
		
	def __str__(self):
		texto = "[{}]\nnombre: {}\ntlf: {}".format("T",self.nombre,self.tlf)
		if self.email:
			texto += "\ne-mail: {}".format(self.email)
		texto += "\nempresa: {}".format(self.empresa)
		return texto